from setuptools import setup, find_packages

setup(
	name = 'en',

    entry_points="""
    [en.ext]
    multiplate = enext.multiplate.plugin:Multiplate
    """
    )
