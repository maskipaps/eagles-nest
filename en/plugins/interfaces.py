from pyutilib.component.core import Interface as _pca_interface
from inspect import  isclass

__all__ = [
	'Interface',
	'Instrument',
	'DataStore'
]

class Interface(_pca_interface):
	"""  insert some additional classmethod here """

class Instrument(Interface):

	def extract_result(context,data_dict):
		""" returns a dictionary containing the status of each test items """

	def before_transform_result(context,data_dict):
		""" do this before transforming the data """

	def transform_result(context,data_dict):
		""" returns the transformation of the result"""

	def before_load_result(context,data_dict):
		""" do this before load the data """

	def load_result(context, data_dict):
		""" returns the transformed data """

class DataStore(Interface):

	def before_create(context,data_dict):
		return data_dict

	def create(context,data_dict):
		pass

	def before_update(context,data_dict):
		return data_dict

	def update(context,data_dict):
		pass
