import logging
from pyutilib.component.core import ExtensionPoint as PluginImplementations
from pyutilib.component.core import Plugin as _pca_plugin
from pyutilib.component.core import SingletonPlugin as _pca_singleton_plugin
from pyutilib.component.core import implements
from pkg_resources import iter_entry_points

import interfaces

__all__ = [
	'PluginNotFoundException',
	'load_all',
	'find_plugins',
	'load',
	'implements',
	'PluginImplementations',
	'Plugin',
	'SingletonPlugin'
]

log  = logging.getLogger(__name__)

PLUGIN_ENTRY_POINT = 'en.ext'

_PLUGINS = []

class PluginNotFoundException(Exception):
	"""
	Raised when a plugin cannot be found
	"""

class Plugin(_pca_plugin):
	"""
	Base class for plugins which require multiple instances.
	"""

class SingletonPlugin(_pca_singleton_plugin):
	'''
    Base class for plugins which are singletons (ie most of them)

    One singleton instance of this class will be created when the plugin is
    loaded. Subsequent calls to the class constructor will always return the
    same singleton instance.
    '''

def load_all():
	plugin_names = find_plugins()
	load(*plugin_names)

def find_plugins():
	"""	Loads all the plugins available in the system """
	eps = []
	for entrypoint in iter_entry_points(PLUGIN_ENTRY_POINT):
		eps.append(entrypoint.name)
	return eps

def load(*plugin_name):
	plugins = []
	for pn in plugin_name:
		plugins.append(_get_service(pn))

	if len(plugins) == 1:
		return plugins[0]
	return plugins

def _get_service(plugin_name):
	ep = iter_entry_points(group=PLUGIN_ENTRY_POINT,name=plugin_name)
	for iterator in ep:
		if iterator:
			plugin = iterator.load()()
			return plugin
	raise PluginNotFoundException(plugin_name)
