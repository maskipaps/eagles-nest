import unittest
import en.plugins as p
import en.tests.mocks as m

class TestInstrument(unittest.TestCase):

	def setUp(self):
		p.load_all()

	def test_01_load_instruments(self):
		plugins  = p.find_plugins()
		assert plugins != None
		assert len(plugins) > 0

	def test_02_get_all_instruments(self):
	 	instruments = p.PluginImplementations(p.Instrument)
		assert instruments != None
		assert len(instruments) > 0
		for i in instruments:
			assert hasattr(i,'extract_result')
			assert hasattr(i,'transform_result')
			assert hasattr(i,'load_result')

	def test_03_extract_data_from_file(self):
		instrument_plugin = p.load('multiplate')
		context = {}
		data_dict = {'path': m.instrument_config['extract_path']}
		json_data = instrument_plugin.extract_result(context,data_dict)
		assert json_data != None
