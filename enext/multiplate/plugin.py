import en.plugins as p

class Multiplate(p.SingletonPlugin):

	p.implements(p.Instrument)

	def __init__(self):
		pass

	def extract_result(self,context,data_dict):
		return 'data'

	def transform_result(self,context,data_dict):
		pass

	def load_result(self,context,data_dict):
		pass
